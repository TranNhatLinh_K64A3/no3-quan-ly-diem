# no3-quan-ly-diem



## Getting started
1. Install IDE (VSCode, ...) 
2. Install Xampp. 
3. Clone project: `git clone https://gitlab.com/TranNhatLinh_K64A3/no3-quan-ly-diem.git`

## File structure

```
common
└── ...
configs
└── ...
controllers
└── ...
models
└── ...
views
└───web
│   └───css
│   └───img
│   └───js
│   └─── ...
(Continue update...)
```
(Example)
| Item           | Explaint |
| -------------- | -------- |
| **main.dart**: | the "entry point" of program. |
| **assets**:    | store static assests like fonts and images. |
| **common**:    | contain colors, textStyle, theme, ... |
| **configs**:   | hold the configs of your application. |
| **database**:  | container database helper class |
| **l10n**:      | contain all localized string. [See more](https://flutter.dev/docs/development/accessibility-and-localization/internationalization) |
| **models**:    | contain entity, enum, .. |
| **networks**:  |
| **router**:    | contain the route navigation |
| **repositories**:    | contain repository |
| **ui**         |  |
| **utils**      |  |
